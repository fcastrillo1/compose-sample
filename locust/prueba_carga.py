# locust -f hydra-locust-alb-single.py

from locust import HttpLocust, TaskSet, seq_task, task, between
import json, time

ENDPOINTS = {
    'HOST'    : 'http://app:8080',
    'CLIENTS' : {
        'HOLA' : {
            'URI': '/hola'
        },
        'SALUDO' : {
            'URI': '/saludos',
            'DATA': dict(nombre='Mike')
        },
        'ADIOS' : {
            'URI': '/adios'
        }
    }
}


# ----------------------------------------------------------------------------------------------------

class HolaTasks(TaskSet):
    token = None

    def on_start(self):
        self.hola()

    @task(1)
    @seq_task(1)
    def hola(self): 
        with self.client.get(
            verify  = False,
            name    = 'HOLA',
            url     = ENDPOINTS['CLIENTS']['HOLA']['URI'],
        ) as response:
            if response.status_code != 200:
                response.failure("HTTP ErrorCode")
            print(response.content)

    @task(1)
    @seq_task(1)
    def getTokenPass(self): 
        with self.client.post(
            verify  = False,
            name    = 'SALUDO',
            url     = ENDPOINTS['CLIENTS']['SALUDO']['URI'],
            headers = {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data    = ENDPOINTS['CLIENTS']['SALUDO']['DATA']
        ) as response:
            if response.status_code != 200:
                response.failure("HTTP ErrorCode")
            print(response.content)

    @task(2)
    @seq_task(2)
    def adios(self): 
        with self.client.get(
            verify  = False,
            name    = 'ADIOS',
            url     = ENDPOINTS['CLIENTS']['ADIOS']['URI'],
            data    = {}
        ) as response:
            if response.status_code != 200:
                response.failure("HTTP ErrorCode")
            print(response.content)



class Cliente1(HttpLocust):
    task_set = HolaTasks
    host     = ENDPOINTS['HOST']
    wait_time = between(0.500, 01.600)
    stop_timeout = 300

class Clienet2(HttpLocust):
    task_set = HolaTasks
    host     = ENDPOINTS['HOST']
    wait_time = between(0.500, 0.600)
    stop_timeout = 300
